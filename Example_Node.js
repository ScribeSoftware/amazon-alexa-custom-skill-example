/*
This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants

   you a nonexclusive copyright license to use all programming code examples from which you can generate similar

   functionality tailored to your own specific needs.

 

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without

   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 

   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for

   a particular purpose are expressly disclaimed.

*/




    var https = require('https');
    var outputText = "";
    var quickReply = "";
    var body; 
    var category = ""; 
    var path = ""; 
    var data; 

//====================================================== START Context Processor Code for Events & Intents ==================================================================== 
    exports.handler = (event, context) => {
    
    try {

                if (event.session.new) {
                // New Session
                console.log("NEW SESSION");
                }
        
        
                switch (event.request.type) {

                    //Begin Launch  Request
                    case "LaunchRequest":
                        console.log('LAUNCH REQUEST');
                        outputText = "";
                        outputText = "<speak> Welcome to Scribe Simple Category Demonstration <p> What are you interested in? </p> </speak>";
                        context.succeed(generateResponse(buildSpeechletResponse(outputText, false),{} )); 
                    break;
                    //End Launch  Request

                    //Begin Intent Request
                    case "IntentRequest":
                    console.log('INTENT REQUEST'); 
            
                            switch(event.request.intent.name) {

                                case "getHelp": 
                                    console.log('getHelp'); 
                                    quickStrategy = "I can respond to Get Customers by Category"; 
                                    quickReply = '<speak>' + quickStrategy + '</speak>';
                                    outputText = quickReply;
                                    context.succeed(generateResponse(buildSpeechletResponse(outputText, false),{} )); 
                                break; 
                               
                                case "getCategory":
                                    var category = event.request.intent.slots.Category.value;
                                    outputText = ""; 
                                    //category = "Water Sports"
                                    var path="/v1/orgs/24624/requests/3431?accesstoken=af8d8b8c-ddbc-4535-b156-da98b96b107e";
                                    //Replace the above PATH with your Scribe Online Request Reply URL
                                    //Post the Path and the Context   
                                    getCustomersByCategory(path, category, context);
                                break;

                                
                                default:
                                    outputText = "<speak> I don't handle that question yet";
                                    context.succeed(generateResponse(buildSpeechletResponse(outputText, false),{} )); 
                               break; 

                        }

                        break; 
            
        
                    // Session End Request
                    case "SessionEndedRequest":
                        console.log('SESSION ENDED REQUEST'); 
                    break;
        
            default:
                context.fail(`INVALID REQUEST TYPE: ${event.request.type}`); 
            }

    }
    catch(error) { context.fail(`Exception ${error}`) }  
};
//======================================================  Exit Context Processor Code for Events & Intents ====================================================================

//========================================================== Format & Response & Processing Functions =========================================================================


function getCustomersByCategory(jobPath, category, context) {
    var outputText;
    var returnValue = ""; 
    // An object of options to indicate where to post to
    var post_options = {
    host: 'endpoint.scribesoft.com',
    path: jobPath,
    method: 'POST',
        
    headers: {
        'Content-Type': 'application/json'
        }
    };
    
    // Set up the request
    
    var body = ""; 
    var returntext; 
    var numberofRecords; 
    
    var post_req = https.request(post_options, function(res) {

        console.log("NOW CALLING..."); 
        console.log("getCustomersByCategory " + jobPath); 
        console.log("Category " + category); 
        
        res.setEncoding('utf8');
      
        res.on('data', function (chunk) {
        console.log('Response: ' + data);
            body +=chunk
        });
        
            res.on('error', function(e) {
            console.log('problem with request: ' + e.message);
            });
        
            res.on('end', () => {
                console.log(body); 
            var data = JSON.parse(body); 
            //Check to see is Scribe returned an empty set
            if (Object.keys(data).length !== 0 ){
                numberofRecords = data.data.length; 
            }
            else {
                numberofRecords = 0;
            }

            //Evaluate all Customers that meet the category and prepare a nice response string  
            var customers = []; 
            var customer; 
        
            if (numberofRecords > 0) {
                outputText = "<speak> <p> Number of customers that match " +  category +  " is " + numberofRecords + "</p> here are the customers,  <p> " 
            
                for (var i = 0; i < data.data.length; i++){
                    customer = data.data[i]; 
                    outputText += '<break time="1s"/>' + customer.firstname + " " + customer.lastname  + " "
                    //Adjust the object.name in the line above to match the field names you wish to have Alexa tell you about from the Build Reply block. 
                }
            }
            else{
            outputText = "<speak> <p> No Customers match this category" ; 
            }


            outputText += "</p></speak>"
            context.succeed(generateResponse(buildSpeechletResponse(outputText,false),{}));
        }); 
    });
   
    
    post_req.write('{category: "' + category + '"}');
    post_req.end();

        
}


//========================================================== END Format & Response & Processing Functions ====================================================================

//================================================================ Utility Processing Functions ==============================================================================

//Speech Functions 
buildSpeechletResponse = (outputText, shouldEndSession) => {

    return {

        "outputSpeech": {

        "type": "SSML",

        "ssml": outputText

        },

        shouldEndSession: shouldEndSession

    }; 

    }; 
    generateResponse = (speechletResponse, sessionAttributes) => {

    return {

        version: "1.0",

        sessionAttributes: sessionAttributes,

        response: speechletResponse

    }; 
    }; 

   
// Extend the default Number object with a formatMoney() method:
// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
// defaults: (2, "$", ",", ".")
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 0;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};
//================================================================== END Processing Functions ================================================================================

